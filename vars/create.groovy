def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Создаем набор микросервисов"
    new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            clickHouseUrl:config.clickHouseUrl,
            clickHouseLogin:config.clickHouseLogin,
            clickHousePassword:config.clickHousePassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            lokiUrl:config.lokiURL,
            lokiAuth:config.lokiAuth,
            gitToken:config.gitToken).k8sCreateServices(env.ProfileName,config.runIdForServices,config.processProfileJSON);
    echo "Выполнено успешно"

    echo "Ожидаем пока поднимуться, 1 минуту."
    new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).jenkinsWaitBuffer();
    echo "Выполнено успешно"


    currentBuild.result = 'SUCCESS' //FAILURE to fail
    return this;
}