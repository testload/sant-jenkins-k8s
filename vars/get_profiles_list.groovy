def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список профилей"

    def list=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).gitlabWikiGetProfilesList()

    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def profilesList='';
    list.each{
        profilesList=profilesList+it.toString()+'\n';
    }

    return profilesList;
}