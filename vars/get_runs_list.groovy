def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    echo "Получаем список результатов для "+env.ProfileName

    def list=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            clickHouseUrl:config.clickHouseUrl,
            clickHouseLogin:config.clickHouseLogin,
            clickHousePassword:config.clickHousePassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).clickHouseGetRunsListByProfileName(env.ProfileName)

//    def list=new TestLoad(script:this,
//            influxHost:config.influxHost,
//            influxPort:config.influxPort,
//            influxLogin:config.influxLogin,
//            influxPassword:config.influxPassword,
//            clickHouseUrl:config.clickHouseUrl,
//            clickHouseLogin:config.clickHouseLogin,
//            clickHousePassword:config.clickHousePassword,
//            gitlabWikiHost:config.gitlabWikiHost,
//            gitlabWikiProjectId:config.gitlabWikiProjectId,
//            jmeterBaseImage:config.jmeterBaseImage,
//            k8sAPIURL:config.k8sAddress,
//            k8sToken:config.k8sToken,
//            k8sCAcert:config.k8sCAcert,
//            loaderId:config.loaderId,
//            gitToken:config.gitToken).influxDBGetRunsListByProfileName(env.ProfileName)
    echo "Найдены успешно"
    currentBuild.result = 'SUCCESS' //FAILURE to fail
    def runsList='';
    list.each{
        runsList=runsList+it.toString()+'\n';
    }

    return runsList;
}