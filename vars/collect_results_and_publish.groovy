def call(body) {

    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()
    echo "Ожидаем(иногда пакетные отправки не полностью обрабатываются до сбора статистики)"
    new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).jenkinsWaitBuffer()
    echo "Завершено ожидание"

    childName=env.ProfileName+"_"+config.usedRunId;

    echo "Получаем статистику"
    def resultJSONBody=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            clickHouseUrl:config.clickHouseUrl,
            clickHouseLogin:config.clickHouseLogin,
            clickHousePassword:config.clickHousePassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).clickHouseCollectResults(env.ProfileName,config.usedRunId,config.usedProfileJSON)
    echo "Получена успешно"
    echo "Собираем тело дочерней страницы"
    def childHTMLBody=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).gitlabWikiBuildChildPageBody(resultJSONBody,env.Commentary,env.UserName)
    echo "Создано успешно"
    echo "Публикуем дочернюю страницу"
    def resultCode=new TestLoad(script:this,
            influxHost:config.influxHost,
            influxPort:config.influxPort,
            influxLogin:config.influxLogin,
            influxPassword:config.influxPassword,
            gitlabWikiHost:config.gitlabWikiHost,
            gitlabWikiProjectId:config.gitlabWikiProjectId,
            jmeterBaseImage:config.jmeterBaseImage,
            k8sAPIURL:config.k8sAddress,
            k8sToken:config.k8sToken,
            k8sCAcert:config.k8sCAcert,
            loaderId:config.loaderId,
            gitToken:config.gitToken).gitlabWikiPublishChildPage(childName, childHTMLBody)
    echo "Создано успешно"
    if (resultCode.toString().equals("200")||resultCode.toString().equals("201")) currentBuild.result = 'SUCCESS' else currentBuild.result = 'FAILURE' //FAILURE to fail
    return resultCode;
}